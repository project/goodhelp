Good Help
---------

This module provides a feature that facilitates the management of Help 
content in a D7 site, to be served up via Services calls to other sites.

If you are interested helping build a Help system for Drupal 8, please 
check out the efforts of the Documentation team.

 * Specs: http://drupal.org/node/1095012
 * Issue: http://drupal.org/node/1031972

On the other hand, if you want to help assemble something for more 
immediate use, feel free to pitch in. I'm especially interested in 
anything that aligns this module with the Doc team's plans.
